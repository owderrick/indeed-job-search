# -*- coding: utf-8 -*-
import requests, time, os.path, sys
from bs4 import BeautifulSoup
from os import listdir, path

def indeed_job_lookup(position, location, exp_lvl, job_salary, job_type):
	count = 0 # number of job postings searched
	no_dup_count = 0 # number of job postings that are not duplicates from previous searches
	dup_count = 0 # number of job postings that are duplicates from previous searches
	duplicate = False
	date_time = time.strftime("%Y_%m_%d-%H_%M_%S") + ".txt"
	new_file = "job_search_on_" +(date_time) # file_name = Year_Month_Day-HourMinuteSecond
	dest = os.getcwd() # get current working directory

	if not (path.isfile(dest + "/" + new_file)): # check if file already exists
		job_file = open(dest + "/" + new_file, "w") 

	job_file.write("***----------------- Initializing search -----------------***\n")
	job_file.write("\n")

	print "***----------------- Initializing search -----------------***\n"

	# ***------- Checks up to 100 pages for job listings -------*** #
	for page in range(1,101):
		indeed_page = (page - 1) * 10 
		url = 'https://www.indeed.com/jobs?q=' +position +job_salary +'&jt=' +job_type +'&l=' +location +'&explvl=' +exp_lvl +'&sort=date' +'&start=' +str(indeed_page)
		
		try:
			site = requests.get(url)
		except Exception as e:
			print(e.message)
			return

		site_info = site.content
		soup = BeautifulSoup(site_info, "lxml")

		# ***------- check for job listings but does not check for sponsored job postings -------*** #
		for job_info in soup.find_all('div', attrs={'data-tn-component': 'organicJob'}):
			count += 1 # increment after each job listing checked
			company = job_info.find('span', {'class': 'company'}).text.strip()
			title = job_info.find('a', attrs={'class': 'turnstileLink', 'data-tn-element': 'jobTitle'}).text.strip()
			link = "https://www.indeed.com" + str(job_info.a.get('href'))
			date_posted = job_info.find('span', {'class': 'date'}).string
			date_posted = str(date_posted).lower()
			if date_posted == "just posted":
				date_posted = "just now"

			# ***----------------- Print the results --------------*** #
			# print "url: " + (url) # uncomment this line to find where error occurs for testing
			print "Job Posting #" + str(count)
			print "Company: " + (company)
			print "Position: " + (title)
			print "Posted " + (date_posted)
			print "Apply at " + (link)
			print

			# ***------- Check for duplicate job findings with previous search files-------*** #					
			for filename in listdir(dest):
				if (filename.startswith("job_search_on_") and filename.endswith('.txt')):
					with open(dest + "/" + filename) as currentFile:
						text = currentFile.read()
						if (link in text):
							duplicate = True
							dup_count += 1
							break
		
			if duplicate is not True:
				no_dup_count += 1	
				# ***------------------ Write the results to job_file.txt------------------*** #
				job_file.write("Job Posting #" +str(no_dup_count) + "\n")
				job_file.write("Company: " + (company.encode('utf8')) + "\n")
				job_file.write("Position: " + (title.encode('utf8')) + "\n")
				job_file.write("Posted " + (date_posted) + "\n")
				job_file.write("Apply at " + (link) + "\n")
				job_file.write("\n")
				
		# ***------------------find if the next button is on the page ------------------*** #
		next_page = False

		for work in soup.find_all('span', attrs={'class': 'np'}):
			list_item = unicode(work.string).encode('utf-8').replace('« ', '').replace('»', '').replace('\xc2\xa0','')
			if list_item == "Next":
				next_page = True

		if next_page == False:
			break;
		
	print "***------------------ Reached end of search ------------------***\n"
	job_file.write("***------------------ Reached end of search ------------------***\n")

	if (dup_count == 1):
		job_file.write("\nThere was %d duplicate match omitted from this list\n" % (dup_count))
	else:
		job_file.write("\nThere were %d duplicate matches omitted from this list\n" % (dup_count))

	job_file.close()

if __name__ == '__main__':
	print("\nAnswer the following questions to find your dream job! The script will also write a text file in your current directory to filter identical " \
		  "job posts by using previous searches. Note that the first search may result in duplicate job postings due to the nature of the Indeed website.")
	
	position = raw_input("\nWhat job position are you looking for?\n").replace(" ", "+").strip()
	location = raw_input("\nWhat location would you like to search?\n").replace(" ", "+").strip()
	exp_lvl = raw_input("\nEnter experience level: Entry, Mid, or Senior?\n").lower().strip() +'_level'
	job_salary = '+$' + str("{:,}".format(input("\nWhat is your expected salary? (Include only digits)\n"))).strip() # also adds necessary commas for thousands
	job_type = raw_input("\nEnter experience level: (Full-time, Internship, Contract, Part-Time, Temporary, or Commission)?\n").replace(" ", "").replace("-", "").strip().lower()

	indeed_job_lookup(position, location, exp_lvl, job_salary, job_type)
