Version 1.0
 
This script searches Indeed after asking the user for relevant information (job position, location, experience 
level, salary, and job type). The job listings are automatically sorted by date posted. It displays what it finds
on the screen, and also writes a text file in the current directory with the same information for ease of access. 
Future searches will reference previous searches in the directory they were created and will filter duplicate 
results. Note that the first search may result in duplicate job postings due to the nature of the Indeed website.